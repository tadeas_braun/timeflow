package cz.tedsoft.timeflow;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {
    TimeflowDataSource dataSource;
    List<DatabaseModel> databaseModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String theme = sharedPrefs.getString("theme", "1");

        if (theme.equals("2")) {
            setTheme(R.style.AppTheme_Add_Dark);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        int position = bundle.getInt("position");
        boolean isCountdown = bundle.getBoolean("is_countdown");

        openDB();

        databaseModel= new ArrayList<DatabaseModel>();
        databaseModel = dataSource.getAllRows(isCountdown);

        int color = databaseModel.get(position).getColor();
        String title = databaseModel.get(position).getName();
        String date = databaseModel.get(position).getDate();
        String time = databaseModel.get(position).getTime();
        int dateFormat = databaseModel.get(position).getDateFormat();
        int timeFormat = databaseModel.get(position).getTimeFormat();

        String[] formatArray = FormatSelector.select(dateFormat, timeFormat);

        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(formatArray[0]);
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern(formatArray[1]);

        int colorPrimaryDark = ColorPrimaryDark.colorPrimaryDark(color, this);

        toolbar.setBackgroundColor(color);
        getSupportActionBar().setTitle(title);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimaryDark);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeDB();
    }

    private void openDB() {
        dataSource = new TimeflowDataSource(this);
        dataSource.open();
    }

    private void closeDB() {
        dataSource.close();
    }

}
