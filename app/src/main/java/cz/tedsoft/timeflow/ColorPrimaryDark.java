package cz.tedsoft.timeflow;

import android.content.Context;
import android.support.v4.content.ContextCompat;

/**
 * Created by tedik on 16.1.2016.
 */
public class ColorPrimaryDark {
    public static int colorPrimaryDark(int color, Context context) {

        int colorPrimaryDark = 0;

        if (color == ContextCompat.getColor(context, R.color.colorPrimaryRed)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkRed);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryPink)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkPink);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryPurple)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkPurple);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryDeepPurple)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkDeepPurple);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryIndigo)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkIndigo);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryBlue)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkBlue);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryLightBlue)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkLightBlue);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryCyan)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkCyan);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryTeal)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkTeal);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryGreen)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkGreen);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryLightGreen)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkLightGreen);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryLime)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkLime);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryYellow)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkYellow);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryAmber)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkAmber);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryOrange)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkOrange);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryDeepOrange)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkDeepOrange);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryBrown)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkBrown);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryGrey)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkGrey);
        } else if (color == ContextCompat.getColor(context, R.color.colorPrimaryBlueGrey)) {
            colorPrimaryDark = ContextCompat.getColor(context, R.color.colorPrimaryDarkBlueGrey);
        }

        return colorPrimaryDark;
    }
}
