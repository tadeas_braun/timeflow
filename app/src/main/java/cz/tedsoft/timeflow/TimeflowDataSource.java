package cz.tedsoft.timeflow;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class TimeflowDataSource {
    private SQLiteDatabase database;
    private DatabaseHelper databaseHelper;
    private String[] allColumns = {DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME, DatabaseHelper.COLUMN_DATE, DatabaseHelper.COLUMN_TIME, DatabaseHelper.COLUMN_COLOR, DatabaseHelper.COLUMN_DATE_FORMAT, DatabaseHelper.COLUMN_TIME_FORMAT};

    public TimeflowDataSource(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public void open() {
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();
    }

    public DatabaseModel insertRow(String name, String date, String time, int color, int dateFormat, int timeFormat, boolean isCountdown) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_NAME, name);
        values.put(DatabaseHelper.COLUMN_DATE, date);
        values.put(DatabaseHelper.COLUMN_TIME, time);
        values.put(DatabaseHelper.COLUMN_COLOR, color);
        values.put(DatabaseHelper.COLUMN_DATE_FORMAT, dateFormat);
        values.put(DatabaseHelper.COLUMN_TIME_FORMAT, timeFormat);

        Cursor cursor;

        if (isCountdown) {
            long insertId = database.insert(DatabaseHelper.TABLE_COUNTDOWN, null, values);
            cursor = database.query(DatabaseHelper.TABLE_COUNTDOWN, allColumns, DatabaseHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
        } else {
            long insertId = database.insert(DatabaseHelper.TABLE_TIMESINCE, null, values);
            cursor = database.query(DatabaseHelper.TABLE_TIMESINCE, allColumns, DatabaseHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
        }

        cursor.moveToFirst();
        DatabaseModel databaseModel = cursorToDatabaseModel(cursor);
        cursor.close();

        return databaseModel;
    }

    public void deleteRow(DatabaseModel databaseModel, boolean isCountdown) {
        long id = databaseModel.getId();
        if (isCountdown) {
            database.delete(DatabaseHelper.TABLE_COUNTDOWN, DatabaseHelper.COLUMN_ID + " = " + id, null);
        } else {
            database.delete(DatabaseHelper.TABLE_TIMESINCE, DatabaseHelper.COLUMN_ID + " = " + id, null);
        }
    }

    public List<DatabaseModel> getAllRows(boolean isCountdown) {
        List<DatabaseModel> databaseModelList = new ArrayList<DatabaseModel>();

        Cursor cursor;
        if (isCountdown) {
            cursor = database.query(DatabaseHelper.TABLE_COUNTDOWN, allColumns, null, null, null, null, null);
        } else {
            cursor = database.query(DatabaseHelper.TABLE_TIMESINCE, allColumns, null, null, null, null, null);
        }

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            DatabaseModel databaseModel = cursorToDatabaseModel(cursor);
            databaseModelList.add(databaseModel);
            cursor.moveToNext();
        }

        cursor.close();
        return databaseModelList;
    }

    private DatabaseModel cursorToDatabaseModel(Cursor cursor) {
        DatabaseModel databaseModel = new DatabaseModel();
        databaseModel.setId(cursor.getLong(0));
        databaseModel.setName(cursor.getString(1));
        databaseModel.setDate(cursor.getString(2));
        databaseModel.setTime(cursor.getString(3));
        databaseModel.setColor(cursor.getInt(4));
        databaseModel.setDateFormat(cursor.getInt(5));
        databaseModel.setTimeFormat(cursor.getInt(6));

        return databaseModel;
    }
}
