package cz.tedsoft.timeflow;

public class FormatSelector {
    public static String[] select(int dateFormat, int timeFormat) {
        String dateFormatText = null;
        switch (dateFormat) {
            case 1:
                dateFormatText  = "dd.MM.yyyy";
                break;
            case 2:
                dateFormatText = "dd/MM/yyyy";
                break;
            case 3:
                dateFormatText = "dd–MM–yyyy";
                break;
            case 4:
                dateFormatText = "yyyy/MM/dd";
                break;
            case 5:
                dateFormatText = "yyyy–MM–dd";
                break;
            case 6:
                dateFormatText = "MM/dd/yyyy";
                break;
            case 7:
                dateFormatText = "yyyy–MM–dd";
                break;
            case 8:
                dateFormatText = "dd MMMM yyyy";
                break;
            case 9:
                dateFormatText = "MMMM dd, yyyy";
                break;
        }

        String timeFormatText = null;
        switch (timeFormat) {
            case 1:
                timeFormatText = "hh:mm a";
                break;
            case 2:
                timeFormatText = "HH:mm";
                break;
        }

        String[] formatArray = new String[] {dateFormatText, timeFormatText};

        return formatArray;
    }
}
