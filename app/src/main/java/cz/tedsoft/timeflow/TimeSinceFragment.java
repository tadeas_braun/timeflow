package cz.tedsoft.timeflow;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class TimeSinceFragment extends Fragment {

    TimeflowDataSource dataSource;
    List<DatabaseModel> databaseModel;
    RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;


    public TimeSinceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_time_since, container, false);
        openDB();

        recyclerView = (RecyclerView) layout.findViewById(R.id.timeSinceRecyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(getActivity(), databaseModel, false);
        recyclerView.setAdapter(adapter);

        return layout;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeDB();
    }

    private void openDB() {
        dataSource = new TimeflowDataSource(getActivity());
        dataSource.open();
        databaseModel = new ArrayList<DatabaseModel>();
        databaseModel = dataSource.getAllRows(false);
    }

    private void closeDB() {
        dataSource.close();
    }
}
