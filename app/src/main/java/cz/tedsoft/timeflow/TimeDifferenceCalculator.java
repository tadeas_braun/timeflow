package cz.tedsoft.timeflow;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeDifferenceCalculator {

    public static int[] difference(String dateText, String timeText, boolean isCountdown, Context context, int dateFormat, int timeFormat) {
        JodaTimeAndroid.init(context);

        int difference;
        int dateTimeFormat = 0;

        String[] formatArray = FormatSelector.select(dateFormat, timeFormat);

        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(formatArray[0]);
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern(formatArray[1]);

        LocalDate localDate = new LocalDate();
        LocalTime localTime = new LocalTime();
        LocalDate date = dateFormatter.parseLocalDate(dateText);
        LocalTime time = timeFormatter.parseLocalTime(timeText);

        if(isCountdown) {
            difference = Days.daysBetween(localDate, date).getDays();

            if(difference < 3) {
                difference = Hours.hoursBetween(localTime, time).getHours();
                dateTimeFormat = 1;
                if (difference < 1) {
                    difference = Minutes.minutesBetween(localTime, time).getMinutes();
                    dateTimeFormat = 2;
                }
            }
        } else {
            difference = Days.daysBetween(date, localDate).getDays();

            if(difference < 3) {
                difference = Hours.hoursBetween(time, localTime).getHours();
                dateTimeFormat = 1;
                if (difference < 1) {
                    difference = Minutes.minutesBetween(time, localTime).getMinutes();
                    dateTimeFormat = 2;
                }
            }
        }
        return new int[] {difference, dateTimeFormat};
    }
}
