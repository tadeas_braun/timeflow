package cz.tedsoft.timeflow;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class AboutActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView aboutList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String theme = sharedPrefs.getString("theme","1");
        if (theme.equals("2")) {
            setTheme(R.style.AppTheme_Dark_NoActionBar);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        aboutList = (ListView) findViewById(R.id.list_about);
        aboutList.setAdapter(new AboutAdapter(this));
        aboutList.setOnItemClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 2) {
            showChangelogDialog();
        }
        else if (position == 3) {
            showCreditsDialog();
        }
        else if (position == 4) {
            openGooglePlusCommunity();
        }
        else if (position == 5) {
            rateApp();
        }
    }

    private void rateApp() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void openGooglePlusCommunity() {
        String googlePlusURL = "https://plus.google.com/communities/104504160658515569476";
        Intent googlePlus = new Intent(Intent.ACTION_VIEW, Uri.parse(googlePlusURL));
        startActivity(googlePlus);
    }

    private void showChangelogDialog() {
        FragmentManager manager = getFragmentManager();
        ChangelogDialogFragment changelogDialogFragment = new ChangelogDialogFragment();
        changelogDialogFragment.show(manager, "changelog");
    }

    private void showCreditsDialog() {
        FragmentManager manager = getFragmentManager();
        CreditsDialogFragment creditsDialogFragment = new CreditsDialogFragment();
        creditsDialogFragment.show(manager, "credits");
    }
}
class SingleRowAbout {
    String title;
    String description;
    SingleRowAbout(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
class AboutAdapter extends BaseAdapter {

    ArrayList<SingleRowAbout> aboutList;
    Context context;
    AboutAdapter(Context c) {
        context = c;
        aboutList = new ArrayList<SingleRowAbout>();
        Resources resources = c.getResources();
        String[] titles = resources.getStringArray(R.array.titles_about);
        String[] descriptions = resources.getStringArray(R.array.descriptions_about);
        for (int position = 0; position < 6; position++) {
            aboutList.add(new SingleRowAbout(titles[position], descriptions[position]));
        }

    }

    @Override
    public int getCount() {
        return aboutList.size();
    }

    @Override
    public Object getItem(int position) {
        return aboutList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = layoutInflater.inflate(R.layout.single_row_about, parent, false);
        TextView title = (TextView) row.findViewById(R.id.textTitleAbout);
        TextView description = (TextView) row.findViewById(R.id.textDescriptionAbout);

        SingleRowAbout temp = aboutList.get(position);

        title.setText(temp.title);
        description.setText(temp.description);

        return row;
    }
}
