package cz.tedsoft.timeflow;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.danlew.android.joda.JodaTimeAndroid;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    static List<DatabaseModel> databaseModel;
    static Context context;
    public static boolean isCountdown;

    RecyclerAdapter(Context context, List<DatabaseModel> databaseModel, boolean countdown) {
        if (countdown) {
            isCountdown = true;
        } else {
            isCountdown = false;
        }

        this.databaseModel = new ArrayList<DatabaseModel>();
        this.context = context;
        this.databaseModel = databaseModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_time_since, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String date = databaseModel.get(position).getDate();
        String time = databaseModel.get(position).getTime();
        int dateFormat = databaseModel.get(position).getDateFormat();
        int timeFormat = databaseModel.get(position).getTimeFormat();
        String differenceText;

        if (isCountdown) {
            int[] difference = TimeDifferenceCalculator.difference(date, time, true, context, dateFormat, timeFormat);

            if (difference[1] == 0) {
                differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_days_remaining);
                if (difference[0] == 1) {
                    differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_day_remaining);
                }
            } else if (difference[1] == 1) {
                differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_hours_remaining);
                if (difference[0] == 1) {
                    differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_hour_remaining);
                }
            } else {
                differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_minutes_remaining);
                if (difference[0] == 1) {
                    differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_minute_remaining);
                }
            }
        } else {
            int[] difference = TimeDifferenceCalculator.difference(date, time, false, context, dateFormat, timeFormat);

            if (difference[1] == 0) {
                differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_days_elapsed);
                if (difference[0] == 1) {
                    differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_day_elapsed);
                }
            } else if (difference[1] == 1) {
                differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_hours_elapsed);
                if (difference[0] == 1) {
                    differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_hour_elapsed);
                }
            } else {
                differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_minutes_elapsed);
                if (difference[0] == 1) {
                    differenceText = String.valueOf(difference[0]) + context.getString(R.string.recycler_view_minute_elapsed);
                }
            }
        }
        String name = databaseModel.get(position).getName();
        holder.nameTextView.setText(databaseModel.get(position).getName());
        holder.differenceTextView.setText(differenceText);
        holder.dateTextView.setText(date);

        int color = databaseModel.get(position).getColor();
        holder.cardView.setCardBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return databaseModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView nameTextView, differenceTextView, dateTextView;
        public CardView cardView;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            nameTextView = (TextView) itemLayoutView.findViewById(R.id.cardTextTitle);
            differenceTextView = (TextView) itemLayoutView.findViewById(R.id.cardTextTimeLeft);
            dateTextView = (TextView) itemLayoutView.findViewById(R.id.cardTextTimeCreated);
            cardView = (CardView) itemLayoutView.findViewById(R.id.timeSinceCardView);
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, DetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("position", getAdapterPosition());
            bundle.putBoolean("is_countdown", isCountdown);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

}
