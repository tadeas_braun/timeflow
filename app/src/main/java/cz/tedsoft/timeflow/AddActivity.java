package cz.tedsoft.timeflow;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.woalk.apps.lib.colorpicker.ColorPickerDialog;
import com.woalk.apps.lib.colorpicker.ColorPickerSwatch;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AddActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    ListView addList;
    Toolbar toolbar;
    int accentColorDialog = -12627531;
    String theme;
    TimeflowDataSource dataSource;
    EditText editName;

    static String time;
    static String date;
    int day,month,yearNew;
    boolean isCountdown = false;
    int dateFormatInt = 1;
    int timeFormatInt = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        theme = sharedPrefs.getString("theme", "1");

        if (theme.equals("2")) {
            setTheme(R.style.AppTheme_Add_Dark);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //noinspection ResourceType
        toolbar.setBackgroundColor(accentColorDialog);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabDone);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editName.getText().toString();
                if (!name.equals("") && !time.equals("") && day != 0 && month != 0 && yearNew != 0) {
                    writeToDatabase(name);
                    finish();
                }
                else {
                    Snackbar.make(view, R.string.snackbar_add, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

        addList = (ListView) findViewById(R.id.list_add);
        addList.setAdapter(new AddAdapter(this));
        addList.setOnItemClickListener(this);

        editName = (EditText) findViewById(R.id.editName);

        setTimeAndDate();

        openDB();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner addSpinner = (Spinner) findViewById(R.id.spinner_add);
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(getApplicationContext(), R.array.spinner_add_array,
                        android.R.layout.simple_spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addSpinner.setAdapter(staticAdapter);
        addSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch(position) {
                    case 0:
                        break;
                    case 1:
                        isCountdown = true;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeDB();
    }

    private void openDB() {
        dataSource = new TimeflowDataSource(this);
        dataSource.open();
    }

    private void closeDB() {
        dataSource.close();
    }

    private void setTimeAndDate() {
        Calendar c = Calendar.getInstance();
        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        time = setTimeFormat(hourString, minuteString);
        yearNew = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
        date = setDateFormat(yearNew, month, day);
    }

    private String setTimeFormat(String hour, String minute) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String timeFormat = sharedPrefs.getString("time_format", "2");

        String time = hour + ":" + minute;

        switch (Integer.parseInt(timeFormat)) {
            case 1:
                DateTimeFormatter hours24 = DateTimeFormat.forPattern("HH:mm");
                DateTimeFormatter hours12 = DateTimeFormat.forPattern("hh:mm a");

                DateTime orig = hours24.parseDateTime(time);
                time = hours12.print(orig);
                timeFormatInt = 1;
                break;
            case 2:
                break;
        }
        return time;
    }

    private String setDateFormat(int year, int month, int day) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String dateFormat = sharedPrefs.getString("date_format", "1");
        String date = null;

        switch (Integer.parseInt(dateFormat)) {
            case 1:
                date = day + "." + month + "." + year;
                break;
            case 2:
                date = day + "/" + month + "/" + year;
                dateFormatInt = 2;
                break;
            case 3:
                date = day + "–" + month + "–" + year;
                dateFormatInt = 3;
                break;
            case 4:
                date = year + "/" + month + "/" + day;
                dateFormatInt = 4;
                break;
            case 5:
                date = year + "–" + month + "–" + day;
                dateFormatInt = 5;
                break;
            case 6:
                date = month + "/" + day + "/" + year;
                dateFormatInt = 6;
                break;
            case 7:
                date = month + "–" + day + "–" + year;
                dateFormatInt = 7;
                break;
            case 8:
                String monthName = getMonth(month);
                date = day + " " + monthName + " " + year;
                dateFormatInt = 8;
                break;
            case 9:
                monthName = getMonth(month);
                date = monthName + " " + day + ", " + year;
                dateFormatInt = 9;
                break;
        }
        return date;
    }

    private String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            String timeFormat = sharedPrefs.getString("time_format", "1");
            boolean timeTwentyFourHour;
            if (timeFormat.equals("1")) {
                timeTwentyFourHour = false;
            }
            else {
                timeTwentyFourHour = true;
            }

            Calendar now = Calendar.getInstance();
            TimePickerDialog tpd = TimePickerDialog.newInstance(
                    AddActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    timeTwentyFourHour
            );
            if (theme.equals("2")) {
                tpd.setThemeDark(true);
            }
            tpd.setAccentColor(accentColorDialog);
            tpd.show(getFragmentManager(), "time_dialog");
        }
        else if (position == 1) {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    AddActivity.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            if (theme.equals("2")) {
                dpd.setThemeDark(true);
            }
            dpd.setAccentColor(accentColorDialog);
            dpd.show(getFragmentManager(), "date_dialog");
        }
        else if (position == 2) {
            final ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
            colorPickerDialog.initialize(getString(R.string.color_dialog_title), new int[]{ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryRed), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryPink), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryPurple), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDeepPurple), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryIndigo), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryBlue), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryLightBlue), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryCyan), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryTeal), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryGreen), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryLightGreen), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryLime), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryYellow), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryAmber), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryOrange), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDeepOrange), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryBrown), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryGrey), ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryBlueGrey)}, ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryIndigo), 4, 2);
            colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {

                @Override
                public void onColorSelected(final int color) {
                    accentColorDialog = color;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

                        toolbarAnim(color);

                        int colorPrimaryDark = ColorPrimaryDark.colorPrimaryDark(color, getApplicationContext());
                        window.setStatusBarColor(colorPrimaryDark);

                    } else {
                        toolbar.setBackgroundColor(color);
                    }
                }
            });

            colorPickerDialog.show(getFragmentManager(), "color_dialog");
        }
    }

    private void toolbarAnim(final int color) {
        Animator animator = ViewAnimationUtils.createCircularReveal(toolbar,
                toolbar.getWidth() / 2,
                toolbar.getHeight() / 2,
                0,
                toolbar.getHeight() * 2);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                toolbar.setBackgroundColor(color);
            }
        });
        animator.setStartDelay(200);
        animator.start();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        day = dayOfMonth;
        month = monthOfYear+1;
        yearNew = year;
        date = setDateFormat(yearNew, month, day);
        addList.invalidateViews();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        String minuteString = minute < 10 ? "0"+minute : ""+minute;
        time = setTimeFormat(hourString, minuteString);
        addList.invalidateViews();
    }

    private boolean writeToDatabase(String name) {
        dataSource.insertRow(name, date, time, accentColorDialog, dateFormatInt, timeFormatInt, isCountdown);
        return true;
    }

}
class SingleRow {
    String title;
    String description;
    int image;

    SingleRow(String title, String description, int image) {
        this.title = title;
        this.description = description;
        this.image = image;
    }
}

class AddAdapter extends BaseAdapter {

    ArrayList<SingleRow> addList;
    Context context;
    AddAdapter(Context c) {
        context = c;
        addList = new ArrayList<SingleRow>();
        Resources resources = c.getResources();
        String[] titles = resources.getStringArray(R.array.titles_add_list);
        String[] descriptions = resources.getStringArray(R.array.descriptions_add_list);
        int[] images = {R.drawable.ic_clock_grey, R.drawable.ic_calendar_grey, R.drawable.ic_palette_grey};
        for (int position = 0; position < 3; position++) {
            addList.add(new SingleRow(titles[position], descriptions[position],images[position]));
        }

    }

    @Override
    public int getCount() {
        return addList.size();
    }

    @Override
    public Object getItem(int position) {
        return addList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = layoutInflater.inflate(R.layout.single_row_add, parent, false);
        TextView title = (TextView) row.findViewById(R.id.textTitle);
        TextView description = (TextView) row.findViewById(R.id.textDescription);
        ImageView image = (ImageView) row.findViewById(R.id.imageView);

        SingleRow temp = addList.get(position);

        String time = AddActivity.time;
        String date = AddActivity.date;

        title.setText(temp.title);
        if (position == 0) {
            description.setText(time);
        }
        else if (position == 1) {
            description.setText(date);
        }
        else {
            description.setText(temp.description);
        }
        image.setImageResource(temp.image);

        return row;
    }
}