package cz.tedsoft.timeflow;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TABLE_TIMESINCE = "time_since";
    public static final String TABLE_COUNTDOWN = "countdown";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_COLOR = "color";
    public static final String COLUMN_DATE_FORMAT = "date_format";
    public static final String COLUMN_TIME_FORMAT = "time_format";

    public static final String DATABASE_NAME = "timeflow";
    public static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_TIMESINCE = "create table " + TABLE_TIMESINCE + "(" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_NAME + " text not null, " + COLUMN_DATE + " text not null, " + COLUMN_TIME + " text not null, " + COLUMN_COLOR + " integer, " + COLUMN_DATE_FORMAT + " integer, " + COLUMN_TIME_FORMAT + " integer );";
    private static final String CREATE_TABLE_COUNTDOWN = "create table " + TABLE_COUNTDOWN + "(" + COLUMN_ID + " integer primary key autoincrement, " + COLUMN_NAME + " text not null, " + COLUMN_DATE + " text not null, " + COLUMN_TIME + " text not null, " + COLUMN_COLOR + " integer, " + COLUMN_DATE_FORMAT + " integer, " + COLUMN_TIME_FORMAT + " integer );";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TIMESINCE);
        db.execSQL(CREATE_TABLE_COUNTDOWN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMESINCE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COUNTDOWN);
        onCreate(db);
    }
}
